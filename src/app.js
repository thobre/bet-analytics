const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");

require("dotenv").config();

const middlewares = require("./middlewares");
const api = require("./api");
const iotFile = require("./iot/iot.js");

const app = express();

app.use(morgan("dev"));
app.use(helmet());

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });

app.get("/schools", (req, res) => {

    iotFile
        .getSchools()
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.send(err);
        });
});

app.get("/isreserved", (req, res) => {

    let classroomName = req.body.classroomName;

    iotFile
        .isReserved(classroomName)
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.send(err);
        });
})

app.post("/reserve", (req, res) => {

    let classroom_id = req.body.classroom_id
    let className = req.body.className;
    let schoolName = req.body.schoolName;
    let startDate = req.body.startDate;
    let endDate = req.body.endDate;

    iotFile
        .reserve(classroom_id, className, schoolName, startDate, endDate)
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.send(err);
        });
});

app.use("/api/v1", api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
