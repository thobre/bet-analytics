// Début Configuration firestore
const admin = require('firebase-admin');
const firebase = require('firebase');
const serviceAccount = require('../../keyfile.json');

try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack);
  }
}
const db = admin.firestore();
// Fin Configuration firestore

const schoolsCollection = db.collection("school");
const classroomsCollection = db.collection("classroom");

require('dotenv').config();

module.exports = {
  getSchools() {
    return new Promise(async (resolve, reject) => {
	
		var schools = {"schools": []};
			
		// ForEach schools
		await schoolsCollection.get().then(async (querySnapShot) => {
			
			var cpt = 1;
			
			querySnapShot.forEach(async (doc) => {

				const data = doc.data();
				var schoolData = {};
				schoolData.label = data.label;
				
				const classesCollection = schoolsCollection.doc(doc.id).collection("class");
				
				// ForEach classes
				await classesCollection.get().then((querySnapShot) => {
					
					var classesTab = [];
					
					querySnapShot.forEach((doc) => {
							
						const data = doc.data();
						classesTab.push({
							label: data.label
						});
					});
					
					schoolData.classes = classesTab;

				}); // Fin ForEach classes
							
				schools.schools.push(schoolData);

				if (querySnapShot.size === cpt) {

					resolve(schools);

				} else {

          cpt++;
        }
			});
		}); // Fin ForEach schools
	
	});
  },
  isReserved(classroomName) {
    return new Promise(async (resolve, reject) => {

        let response = {};
        response.classroomName = classroomName;
        response.isReserved = false;

        // ForEach classrooms
        await classroomsCollection.get().then(async (querySnapShot) => {

            if (querySnapShot.size === 0) {

              resolve(response);
            }

            querySnapShot.forEach(async (doc) => {

                const data = doc.data();
                let docName = data.name;

                if (docName = classroomName) {

                    const queryBookings = classroomsCollection.doc(doc.id).collection("booking");

                    // ForEach bookings
                    await queryBookings.get().then(async (querySnapShot) => {

                        if (querySnapShot.size === 0) {

                          resolve(response);
                        }
                        
                        let cpt = 1;
                        const date = new Date();

                        querySnapShot.forEach((doc) => {

                            const data = doc.data();
                            const docStartDate = data.startDate.toDate();
                            const docEndDate = data.endDate.toDate();

                            if (date >= docStartDate && date <= docEndDate) {

                              response.isReserved = true;
                              response.school = data.school;
                              response.class = data.class;

                              resolve(response);

                            } else{
                              
                                if (querySnapShot.size === cpt) {

                                resolve(response);

                                } else {

                                  cpt++;
                                }
                            }
                        });

                    }); // Fin ForEach bookings
                }
            });

        }); // Fin ForEach classrooms
    });
  },
  reserve(classroom_id, className, schoolName, startDate, endDate) {
    return new Promise(async (resolve, reject) => {

        let docStartDate = new Date(startDate);
        let docEndDate = new Date(endDate);
        let timestampStartDate = firebase.firestore.Timestamp.fromDate(docStartDate);
        let timestampEndDate = firebase.firestore.Timestamp.fromDate(docEndDate);
        let queryBooking = classroomsCollection.doc(classroom_id).collection("booking");

        queryBooking.add({
          className: className,
          schoolName: schoolName,
        })
        .then(async (docRef) => {
          await queryBooking.doc(docRef.id).update({
            startDate: timestampStartDate,
            endDate: timestampEndDate
          });
        });

        resolve("Réservation effectuée");
    });
  }
};
